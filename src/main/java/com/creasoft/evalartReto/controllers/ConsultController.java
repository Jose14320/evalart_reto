package com.creasoft.evalartReto.controllers;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.creasoft.evalartReto.dto.ListFilter;
import com.creasoft.evalartReto.entity.Client;
import com.creasoft.evalartReto.services.IFindData;
import com.creasoft.evalartReto.utils.Utils;


@RestController
@RequestMapping("/findData")
public class ConsultController {
	
	@Autowired(required = true)
	private IFindData findDataService;
	
	Integer type = null;
	String location = null;
	Float minBalance = null;
	Float maxBalance = null;
	String codes = "";
	String response = "";
	List<Client> clients;
	List<Client> clientsAux = new ArrayList<Client>();
	Boolean validCompany = true;
	int count = 0;
	long countMale = 0;
	long countFemale = 0;
	@PostMapping("")
	public String index(@RequestParam("file") MultipartFile file) {
		Utils rt = new Utils();
		
		List<ListFilter> lf = rt.Read(file);
		
		
		lf.forEach(data ->{
			type = null;
			location = null;
			minBalance = null;
			maxBalance = null;
			data.getData().forEach(filter->{
				
				if(filter.getName().equals("TC")) {
					type = Integer.valueOf(filter.getValue());
				}
				if(filter.getName().equals("UG")) {
					location = filter.getValue();
				}
				if(filter.getName().equals("RI")) {
					minBalance = Float.valueOf(filter.getValue());
				}
				if(filter.getName().equals("RF")) {
					maxBalance = Float.valueOf(filter.getValue());
				}
				
			});	
			codes = "";
			count = 0;
			clients = findDataService.findData(type, location, minBalance, maxBalance);
			clientsAux = new ArrayList<Client>();
			clients.forEach(c -> {
				validCompany = true;
				clientsAux.forEach(caux->{
					if(c.getCompany().equals(caux.getCompany())) {
						validCompany = false;
					}
				});
				if(validCompany) {
					clientsAux.add(c);
				}
			});
			
			countMale = clientsAux.stream()
					  .filter(c -> c.getMale())
					  .count();
			countFemale = clientsAux.stream()
					  .filter(c -> !c.getMale())
					  .count();
			response += "<"+data.getName()+">\n";
			if(countMale > countFemale) {
				countFemale = countFemale > 4 ? 4 : countFemale;
				countMale = countFemale > 4 ? 4 : countFemale;
				clientsAux.forEach(caux ->{
					if(caux.getMale()) {
						if(countMale > 0) {
							if(!caux.getEncrypt()) {
								codes += caux.getCode()+",";
							}
							else {
								codes += rt.getCode(caux.getCode())+",";
							}
							count++;
							countMale--;
						}
						
					}
					if(!caux.getMale()) {
						if(countFemale > 0) {
							if(!caux.getEncrypt()) {
								codes += caux.getCode()+",";
							}
							else {
								codes += rt.getCode(caux.getCode())+",";
							}
							count++;
							countFemale--;
						}
						
					}
					
				});
			}
			else {
				countMale = countMale > 4 ? 4 : countMale;
				countFemale = countMale > 4 ? 4 : countMale;
				clientsAux.forEach(caux ->{
					if(caux.getMale()) {
						if(countMale > 0) {
							if(!caux.getEncrypt()) {
								codes += caux.getCode()+",";
							}
							else {
								codes += rt.getCode(caux.getCode())+",";
							}
							count++;
							countMale--;
						}
						
					}
					if(!caux.getMale()) {
						if(countFemale > 0) {
							if(!caux.getEncrypt()) {
								codes += caux.getCode()+",";
							}
							else {
								codes += rt.getCode(caux.getCode())+",";
							}
							count++;
							countFemale--;
						}
						
						
					}
				});	
			}
			if(count < 4)
				codes = "CANCELADA,";
			response += codes.substring(0, codes.length()-1)+"\n";
		});
        System.out.println(response);
		return response;
	}

}
