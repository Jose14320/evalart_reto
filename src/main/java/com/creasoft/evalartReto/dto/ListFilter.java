package com.creasoft.evalartReto.dto;

import java.util.List;

public class ListFilter {
	private String name;
	private List<FilterData> data;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FilterData> getData() {
		return data;
	}
	public void setData(List<FilterData> data) {
		this.data = data;
	}
}
