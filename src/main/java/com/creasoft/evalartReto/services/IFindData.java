package com.creasoft.evalartReto.services;

import java.util.List;

import com.creasoft.evalartReto.entity.Client;

public interface IFindData {

	public List<Client> findData(Integer type, String location, Float minBalance, Float maxBalance);
	
}
