package com.creasoft.evalartReto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.creasoft.evalartReto.dao.IFindDataDao;
import com.creasoft.evalartReto.entity.Client;

@Service
public class FinDataImpl implements IFindData {

	@Autowired
	private IFindDataDao findDataDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Client> findData(Integer type, String location, Float minBalance, Float maxBalance) {
		return (List<Client>) findDataDao.findData(type, location, minBalance, maxBalance);
	}
}
