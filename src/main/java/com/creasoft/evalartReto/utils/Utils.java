package com.creasoft.evalartReto.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import com.creasoft.evalartReto.dto.FilterData;
import com.creasoft.evalartReto.dto.ListFilter;

public class Utils {
	public Utils() {
		
	}
	
	public List<ListFilter> Read(MultipartFile mp){
		Path filepath = Paths.get("./", mp.getOriginalFilename());
		try {
			mp.transferTo(filepath);
		} catch (IllegalStateException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File f = new File(filepath.toString());
		String linea;
	    String texto = "<";
		Scanner entrada = null;
		List<ListFilter> lf = new ArrayList<ListFilter>();
		List<FilterData> ldf = new ArrayList<FilterData>();
		ListFilter dataLF = new ListFilter();
		try {
			entrada = new Scanner(f);
			while (entrada.hasNext()) { 
	            linea = entrada.nextLine();  
	            if (linea.contains(texto)) {
	            	if(dataLF.getName() != null) {
	            		dataLF.setData(ldf);
	            		lf.add(dataLF);
	            		dataLF = new ListFilter();
	            		ldf = new ArrayList<FilterData>();
	            	}
	            	linea = linea.replace("<", "");
	            	linea = linea.replace(">", "");
	            	dataLF.setName(linea);
	            }
	            else {
	            	String[] vect = linea.split(":");
	            	FilterData data = new FilterData();
	            	data.setName(vect[0]);
	            	data.setValue(vect[1]);
	            	ldf.add(data);
	            }
	        }
		} catch (MultipartException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
            System.out.println(e.toString());
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
        	if(dataLF.getName() != null) {
        		dataLF.setData(ldf);
        		lf.add(dataLF);
        		dataLF = new ListFilter();
        		ldf = new ArrayList<FilterData>();
        	}
            if (entrada != null) {
                entrada.close();
            }
        }
		
		return lf;
	}
	
	public String getCode(String encrypt) {
		String code = "";
		try {
			URL url = new URL("https://test.evalartapp.com/extapiquest/code_decrypt/"+encrypt);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
            	code = output.replaceAll("^[\"']+|[\"']+$", "");
            }
            conn.disconnect();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}
}
