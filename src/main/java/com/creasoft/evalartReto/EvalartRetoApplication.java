package com.creasoft.evalartReto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvalartRetoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvalartRetoApplication.class, args);
	}

}
