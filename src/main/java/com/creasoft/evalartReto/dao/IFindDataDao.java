package com.creasoft.evalartReto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.creasoft.evalartReto.entity.Client;

public interface IFindDataDao extends CrudRepository<Client, Integer>{

	@Query(value = "SELECT distinct c.* FROM client c "
			+ "INNER JOIN account a "
			+ "ON c.id = a.client_id "
			+ "WHERE (:type is null or c.type = :type) "
			+ "AND (:location is null or c.location = :location) "
			+ "GROUP BY c.id "
			+ "HAVING (:minBalance is null or SUM(a.balance) >= :minBalance) "
			+ "AND (:maxBalance is null or SUM(a.balance) <= :maxBalance) "
			+ "ORDER BY SUM(a.balance) desc, c.code desc ",
			nativeQuery = true)
	public List<Client> findData(@Param("type") Integer type, 
			@Param("location") String location,
			@Param("minBalance") Float minBalance,
			@Param("maxBalance") Float maxBalance);
}
